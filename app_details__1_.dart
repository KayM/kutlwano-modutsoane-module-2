class App {
  String name = '';
  String category = '';
  String developer = '';
  int year = 0;
  
  String capitalizeWinner() {
    return name.toUpperCase();
  }
  @override
  String toString() {
    return 'Name of App: $name; Category: $category; Developer: $developer; Year won: $year';
  }
}

void main() { 
  final app = App();
  app.name = 'Ambani Africa';
  app.category = 'Best South African solution, Best gaming solution and Best educational solution';
  app.developer = 'Mukundi Lambani';
  app.year = 2021;

  print(app);
  print('The MTN App of the year winner is: ' +  app.capitalizeWinner());
  
}
